import styles from "./product-list-item.module.scss";
import Link from 'next/link';
import { getPrice } from "../../util/currency";

const ProductListItem = ({ item }) => {

	return (
		<Link
			key={item.productId}
			href={{
				pathname: "/product-detail/[id]",
				query: { id: item.productId },
			}}
		>
			<a className={styles.link}>
				<div className={styles.imageContainer}>
					<img src={item.image} alt="" className={styles.image} />
				</div>
				<div className={styles.info}>
					<div className={styles.title}>{item.title}</div>
					<div className={styles.price}>{getPrice(item.price)}</div>
				</div>
			</a>
		</Link>
  );
};

export default ProductListItem;
