# UI Dev Technical Test - Dishwasher App

## Candidate: Tom Dixon

### Assumptions, notes
- I have not used Next.js before, so had to spend some time exploring this and had a selection of next issues
- Had trouble with node-fetch so I've used the mockdata and not the api
- Could not configure jest to work. Focussed instead on the appearance
- I have not made the image carosel, I've just used a single image

### Improvements/TODO
- use mui Collapse or similar library to animate the read more accordion
- Add the image carosel
- Fetch data using API
- The mock data does not appear to be complete - so I've added placeholders to the page for things like description, special offers, and read more section - need to remove these for production
- The images are not particularly suited to the design - more consistency, a transparent background and better cropped images would be preferred
