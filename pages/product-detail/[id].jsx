import ProductCarousel from "../../components/product-carousel/product-carousel";
import mockData from '../../mockData/data.json';
import { ATTRIBUTES_TO_SHOW } from "../../util/config";
import styles from './product-detail.module.scss';
import { getPrice } from "../../util/currency";
import Link from 'next/link';

export async function getServerSideProps(context) {
	const id = context.params.id;
// 	const response = await fetch(
// 		"https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
// 	);
// 	const apiData = await response.json();
	const products = mockData.products || [];
	const data = products.find(item => +item.productId === +id);
	return data 
		? {
			props: { data, id },
		} : {
			notFound: true,
		};
}

const ProductDetail = ({ data, id }) => {
	return (
		<div>
			<h1 className={styles.header}>
				<Link
					key={id}
					href={{
						pathname: "/",
					}}
				>
					<img src="/chevron.svg" className={styles['back']} />
				</Link>
				<span className={styles['header-text']}>
					{data?.title}
				</span>
			</h1>
			<div className={styles.page}>
				<div className={styles.productImage}>
					<ProductCarousel image={data?.image}/>
				</div>
				<div className={styles.priceSection}>
					<p className={styles['priceSection-price']}>{getPrice(data.price)}</p>
					<p className={styles['priceSection-special']}>{data.displaySpecialOffer || '[special offer here]'}</p>
					<p className={styles['priceSection-guarantee']}>{data.dynamicAttributes?.guarantee}</p>
				</div>

				<div className={styles.detailsSection}>
					<div className={styles.container}>
						<h2>Product information</h2>
						<p className={styles['detailsSection-info']}>
							Product code: {data.code}
							<br />
							{data.description || '[description would go here]'}
						</p>
					</div>
					<details className={styles.detailsAccordion}>
						<summary className={styles['detailsAccordion-summary']}>Read more <img src="/chevron.svg" className={styles['detailsAccordion-chevron']} /></summary>
						<p className={styles['detailsAccordion-content']}>{'[loads more info]'}</p>
					</details>
					<div className={styles.container}>
						<h2 className={styles['detailsSpec-header']}>Product specification</h2>
						<ul className={styles.attributeList}>
							{ATTRIBUTES_TO_SHOW.map((attr) => {
								const attributes = data?.dynamicAttributes || {};
								return attributes?.[attr?.key] && (
									<li key={attr.key} className={styles['attributeList-item']}>
										<span>{attr.label}</span>
										<span>{attributes[attr.key]}</span>
									</li>
								)
							})}
						</ul>
					</div>
				</div>
			</div>
		</div>
  );
};

export default ProductDetail;
