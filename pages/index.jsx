import Head from "next/head";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";
import mockData from '../mockData/data.json';

export async function getServerSideProps() {
	// const response = await fetch(
	// 	"https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
	// );
	// const apiData = await response.json();
	const data = mockData;
	return {
		props: {
		data: data,
		},
	};
}

const Home = ({ data }) => {
  const items = data?.products || [];
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1 className={styles.header}>Dishwashers ({items.length})</h1>
        <div className={styles.content}>
          {items.slice(0,20).map((item) => {
			 return  (
              <ProductListItem item={item} key={item.productId} />
          )})}
        </div>
      </div>
    </div>
  );
};

export default Home;
