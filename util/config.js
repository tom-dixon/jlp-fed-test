export	const ATTRIBUTES_TO_SHOW = [
	{ key: 'adjustableracking', label: 'Adjustable racking Information'},
	{ key: 'childlock', label: 'Child lock Door & control lock Delay Start'},
	{ key: 'timerdelay', label: 'Delay Wash'},
	{ key: 'delicatewash', label: 'Delicate Wash'},
	{ key: 'dimensions', label: 'Dimensions'},
	{ key: 'dryingperformance', label: 'Drying performance'},
	{ key: 'dryingsystem', label: 'Drying system'},
];
