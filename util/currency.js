export const getPrice = price => {
	const monetise = new Intl.NumberFormat(undefined, { style: 'currency', currency: price.currency });
	return monetise.format(price.now);
}
